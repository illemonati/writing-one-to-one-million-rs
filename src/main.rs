use std::fs::File;
use std::io::Write;

fn main() {
    let mut out = File::create("out.txt").expect("file create error");
    for i in 0..1000001 {
        out.write(format!("{}\n", number_to_word(i)).as_bytes()).expect("write error");
    }
}



fn number_to_word(mut number: usize) -> String {
    let units = vec!{"one", "ten", "hundred", "thousand", "million", "billion"};
    let numbers = vec!{"one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};
    let teens = vec!{"ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"};
    let tens = vec!{"ten", "twenty", "thirty", "fourty", "fifty", "sixty", "seventy", "eighty", "ninety"};
    let mut result = String::new();

    if number == 0 {
        return String::from("zero");
    }

    if number >= 1000000000 {
        result += number_to_word(number/1000000000).as_str();
        result += units[5];
        result += " ";
        number -= number/1000000000*1000000000;
    }

    if number >= 1000000 {
        result += number_to_word(number/1000000).as_str();
        result += units[4];
        result += " ";
        number -= number/1000000*1000000;
    }
    if number >= 1000 {
        result += number_to_word(number/1000).as_str();
        result += units[3];
        result += " ";
        number -= number/1000*1000;
    }
    if number >= 100 {
        result += numbers[(number/100-1) as usize];
        result += " ";
        result += units[2];
        result += " ";
        number = number - ((number/100)*100);
    }
    if number >= 10 && number < 20 {
        number = number - 10;
        result += teens[number as usize];
        result += " ";
    } else {
        if number >= 20 {
            result += tens[(number/10 - 1) as usize];
            result += " ";
            number -= number / 10 * 10;
        }
        if number >= 1 {
            result += numbers[(number - 1) as usize];
            result += " ";
        }
    }

    return result;
}


